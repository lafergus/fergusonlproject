class Patient < ActiveRecord::Base
  has_many :appointments
  has_many :physicians, through: :appointments
  has_many :time_slots, through: :appointments

  def full_name
    "#{f_name} #{l_name}"
  end
end
