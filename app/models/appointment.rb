class Appointment < ActiveRecord::Base
  belongs_to :physician
  belongs_to :patient
  belongs_to :time_slot
  belongs_to :user
  has_many :invoices
  has_many :diagnoses, through: :invoices




end
