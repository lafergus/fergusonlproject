class Physician < ActiveRecord::Base
  has_many :appointments
  has_many :patients, through: :appointments
  belongs_to :user

  def full_name
    "#{f_name} #{l_name}"
  end
end
