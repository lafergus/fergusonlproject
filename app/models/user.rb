class User < ActiveRecord::Base
  has_many :invoices
  has_many :appointments, through: :invoices
end
