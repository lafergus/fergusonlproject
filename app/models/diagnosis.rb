class Diagnosis < ActiveRecord::Base
  has_many :invoices
  has_many :appointments, through: :invoices
  belongs_to :user
end
