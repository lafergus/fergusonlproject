json.array!(@patients) do |patient|
  json.extract! patient, :id, :f_name, :l_name, :address
  json.url patient_url(patient, format: :json)
end
