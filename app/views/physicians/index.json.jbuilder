json.array!(@physicians) do |physician|
  json.extract! physician, :id, :f_name, :l_name
  json.url physician_url(physician, format: :json)
end
