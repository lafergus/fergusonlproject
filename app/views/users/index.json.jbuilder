json.array!(@users) do |user|
  json.extract! user, :id, :u_name, :u_title
  json.url user_url(user, format: :json)
end
