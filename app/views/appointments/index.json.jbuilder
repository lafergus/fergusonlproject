json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :patient_id, :physician_id, :time_slot_id, :invoice_id, :reason, :notes, :ap_date
  json.url appointment_url(appointment, format: :json)
end
