json.array!(@invoices) do |invoice|
  json.extract! invoice, :id, :diagnosis_id, :appointment_id, :user_id, :notes
  json.url invoice_url(invoice, format: :json)
end
