json.array!(@diagnoses) do |diagnosis|
  json.extract! diagnosis, :id, :diagnosis_code, :description, :amount
  json.url diagnosis_url(diagnosis, format: :json)
end
