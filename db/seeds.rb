# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = User.create(user_name:'Sally Hanson', user_title: 'Office Worker')
users = User.create(user_name:'Victoria Beckam', user_title: 'System Administrator')

patients = Patient.create(f_name: 'Keith', l_name: 'Harper', address: '123 Farry Lane')
patients = Patient.create(f_name: 'Maranda', l_name: 'Bayless', address: '2323 Love Lane')
patients = Patient.create(f_name: 'Kristin', l_name: 'Moore', address: '2121 Allen Parkway')
patients = Patient.create(f_name: 'Lacey', l_name: 'Straub', address: '6598 Houston Avenue')
patients = Patient.create(f_name: 'Misty', l_name: 'Hall', address: '6552 Taft Road')

physicians = Physician.create(f_name: 'Sarah', l_name: 'Webb')
physicians = Physician.create(f_name: 'Steve', l_name: 'Moor')
physicians = Physician.create(f_name: 'Stephanie', l_name: 'Champion')

diagnoses = Diagnostic.create(diagnostic_code: '919.0', description: 'Abrasion without infection', amount: '29.95')
diagnoses = Diagnostic.create(diagnostic_code: '682.9 ', description: ' Abscess, acute NOS', amount: '31.95')
diagnoses = Diagnostic.create(diagnostic_code: '706.1', description: 'Acne, cystic and vulgaris', amount: '31.95')
diagnoses = Diagnostic.create(diagnostic_code: '702.0 ', description: 'Actinic keratosis', amount: '35.95')
diagnoses = Diagnostic.create(diagnostic_code: '704.01', description: 'Alopecia areata', amount: '35.95')
diagnoses = Diagnostic.create(diagnostic_code: '607.1', description: 'Balanitis', amount: '40.95')
diagnoses = Diagnostic.create(diagnostic_code: '919.4', description: 'Bee sting', amount: '35.95')
diagnoses = Diagnostic.create(diagnostic_code: '529.3', description: 'Black hairy tongue', amount: '40.95')
diagnoses = Diagnostic.create(diagnostic_code: '949.0 ', description: ' Burn, unspecified', amount: '40.95')
diagnoses = Diagnostic.create(diagnostic_code: '700', description: 'Callus', amount: '20.95')
diagnoses = Diagnostic.create(diagnostic_code: '448.9 ', description: 'Capillary disease', amount: '30.95')
diagnoses = Diagnostic.create(diagnostic_code: '078.3', description: 'Cat scratch disease', amount: '25.95')
diagnoses = Diagnostic.create(diagnostic_code: '528.5', description: 'Cheilitis', amount: '20.95')
diagnoses = Diagnostic.create(diagnostic_code: '133.8', description: 'Chigger bites', amount: '25.95')
diagnoses = Diagnostic.create(diagnostic_code: '691.0', description: 'Dermatitis, diaper', amount: '29.95')
diagnoses = Diagnostic.create(diagnostic_code: '459.89', description: 'Ecchymoses', amount: '32.95')
diagnoses = Diagnostic.create(diagnostic_code: '681.01', description: 'Felon', amount: '35.95')
diagnoses = Diagnostic.create(diagnostic_code: '704.8', description: 'Folliculitis', amount: '35.95')
diagnoses = Diagnostic.create(diagnostic_code: '078.19', description: 'Genintal warts', amount: '50.95')
diagnoses = Diagnostic.create(diagnostic_code: '684', description: 'Impetigo', amount: '35.95')
diagnoses = Diagnostic.create(diagnostic_code: '709.09', description: 'Lentigo', amount: '37.95')
diagnoses = Diagnostic.create(diagnostic_code: '703.8', description: 'Nail dystrophy', amount: '40.95')
diagnoses = Diagnostic.create(diagnostic_code: '691.8', description: 'Neurodermatitis', amount: '40.95')
diagnoses = Diagnostic.create(diagnostic_code: '690.8', description: 'Parakeratosis', amount: '45.95')
diagnoses = Diagnostic.create(diagnostic_code: '692.79', description: 'Photodermatitis', amount: '40.95')