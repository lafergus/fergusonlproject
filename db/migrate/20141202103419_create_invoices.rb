class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :diagnosis_id
      t.integer :appointment_id
      t.integer :user_id
      t.string :notes

      t.timestamps
    end
  end
end
