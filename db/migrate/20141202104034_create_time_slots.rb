class CreateTimeSlots < ActiveRecord::Migration
  def change
    create_table :time_slots do |t|
      t.time :s_time
      t.date :s_date

      t.timestamps
    end
  end
end
