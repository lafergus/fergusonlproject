class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :u_name
      t.string :u_title

      t.timestamps
    end
  end
end
