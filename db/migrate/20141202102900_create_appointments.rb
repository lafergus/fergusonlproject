class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :patient_id
      t.integer :physician_id
      t.integer :time_slot_id
      t.integer :invoice_id
      t.string :reason
      t.string :notes
      t.datetime :ap_date

      t.timestamps
    end
  end
end
