class CreateDiagnoses < ActiveRecord::Migration
  def change
    create_table :diagnoses do |t|
      t.string :diagnosis_code
      t.string :description
      t.decimal :amount,    :precision => 5, :scale => 2

      t.timestamps
    end
  end
end
